#!/bin/bash

alphabet="abcdefghijklmnopqrstuvwxyz"
caps="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

hour=$(date +%H)

#Shift 5 ke kiri = shift 21 ke kanan
hour=26-$hour

encoded_alphabet="${alphabet:$hour}${alphabet:0:$hour}"
encoded_caps="${caps:$hour}${caps:0:$hour}"

input_file=/home/richard160421084/Documents/Sisop/Prak1/Tugas/4/"$(date +\%H:\%M) $(date +\%d:\%m:\%Y)"_encrypted.txt

decoded_message=$(cat "$input_file" | while read line; do echo "$line" | tr "$alphabet" "$encoded_alphabet" | tr "$caps" "$encoded_caps" ; done);

echo "$decoded_message" >> /home/richard160421084/Documents/Sisop/Prak1/Tugas/4/"$(date +\%H:\%M) $(date +\%d:\%m:\%Y)"_decrypted.txt


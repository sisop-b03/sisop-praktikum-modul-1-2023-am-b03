#!/bin/bash

cp /var/log/syslog /home/richard160421084/Documents/Sisop/Prak1/Tugas/4/"$(date +\%H:\%M) $(date +\%d:\%m:\%Y)".txt

alphabet="abcdefghijklmnopqrstuvwxyz"
caps="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

hour=$(date +%H)

encoded_alphabet="${alphabet:$hour}${alphabet:0:$hour}"
encoded_caps="${caps:$hour}${caps:0:$hour}"

input_file=/home/richard160421084/Documents/Sisop/Prak1/Tugas/4/"$(date +\%H:\%M) $(date +\%d:\%m:\%Y)".txt

encoded_message=$(cat "$input_file" | while read line; do echo "$line" | tr "$alphabet" "$encoded_alphabet" | tr "$caps" "$encoded_caps" ; done);

echo "$encoded_message" >> /home/richard160421084/Documents/Sisop/Prak1/Tugas/4/"$(date +\%H:\%M) $(date +\%d:\%m:\%Y)"_encrypted.txt

bash log_decrypt.sh


# cron job
#0 */2 * * * /bin/bash /home/richard160421084/Documents/Sisop/Prak1/Tugas/4/log_encrypt.sh

#!/bin/bash

FILE="2023 QS World University Rankings.csv"

echo "a) 5 universitas ranking tertinggi di Jepang"
grep ",Japan," "$FILE" | sort -t "," -k1,1n | head -n 5 | cut -d "," -f 2

echo -e "\nb) 5 universitas dengan skor fsr terendah di Jepang"
grep ",Japan," "$FILE" | sort -t "," -k9,9n | head -n 5 | cut -d "," -f 2

echo -e "\nc) 10 universitas di Jepang dengan rank ger tertinggi"
grep ",Japan," "$FILE" | sort -t "," -k20,20n | head -n 10 | cut -d "," -f 2

echo -e "\nd) Universitas yang mengandung kata 'keren'"
grep -i "keren" "$FILE" | cut -d "," -f 2

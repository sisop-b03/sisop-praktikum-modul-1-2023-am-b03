# No 1
Soal ini menanyakan 4 hal  
a) 5 universitas dengan ranking tertinggi di Jepang  
b) 5 universitas dengan skor fsr terendah di Jepang  
c) 10 universitas di Jepang dengan rank GER tertinggi  
d) Universitas dengan kata kunci keren  

Untuk menyelesaikan permasalahan tersebut, dapat diikuti langkah langkah berikut :  

1) Tambahkan file .csv ke folder saat ini (optional)

2) Buat file `university_survey.sh` dengan menggunakan command  
`nano university_survey.sh`
![buat file script](images/1_1.jpeg)  

3) Tambahkan text berikut ke script tersebut  
```shell
#!/bin/bash

FILE="2023 QS World University Rankings.csv"

echo "a) 5 universitas ranking tertinggi di Jepang"
grep ",Japan," "$FILE" | sort -t "," -k1,1n | head -n 5 | cut -d "," -f 2

echo -e "\nb) 5 universitas dengan skor fsr terendah di Jepang"
grep ",Japan," "$FILE" | sort -t "," -k9,9n | head -n 5 | cut -d "," -f 2

echo -e "\nc) 10 universitas di Jepang dengan rank ger tertinggi"
grep ",Japan," "$FILE" | sort -t "," -k20,20n | head -n 10 | cut -d "," -f 2

echo -e "\nd) Universitas yang mengandung kata 'keren'"
grep -i "keren" "$FILE" | cut -d "," -f 2
```
![isi script](images/1_2.jpeg)  
Keterangan : Apabila file .csv tidak berada di folder yang sama dengan script maka variabel `FILE` harus menggunakan absolute path  

4) Secara default, file tidak memiliki permission untuk dijalankan maka kita harus menambahkan permission untuk execute script tersebut dengan menggunakan command  
`chmod +x university_survey.sh`  
![penambahan permission](images/1_3.jpeg)  

5) Jalankan file tersebut dengan menggunakan command  
`bash university_survey.sh` atau `./university_survey.sh`  
![run script](images/1_4.jpeg)  


Penjelasan code
- Langkah ini merupakan langkah yang opsional. Deklarasikan path ke file .csv tadi menjadi variabel untuk kemudahan pengetikan & pembacaan code  
- Breakdown section :  
`grep ",Japan," "$FILE"` : Mencari substring ",Japan," pada file yang ditunjuk oleh variabel `FILE`  
`grep -i "keren" "$FILE"` : Mencari substring "keren" yang tidak case-sensitive pada file yang ditunjuk oleh variabel `FILE`  
`sort -t "," -k1,1n` : Melakukan pengurutan pada kolom ke-1 (rank) dengan tanda pemisah adalah koma (,) secara numerik  
`sort -t "," -k9,9n` : Melakukan pengurutan pada kolom ke-9 (fsr score) dengan tanda pemisah adalah koma (,) secara numerik  
`sort -t "," -k20,20n` : Melakukan pengurutan pada kolom ke-20 (ger rank) dengan tanda pemisah adalah koma (,) secara numerik  
`head -n 5` : Mengambil 5 data teratas  
`head -n 10` : Mengambil 10 data teratas  
`cut -d "," -f 2` : Hanya menampilkan nilai pada kolom ke-2 (nama universitas) dengan tanda pemisah adalah koma (,)  
- Untuk soal (a), pertama kita cari dahulu universitas yang berada di Jepang, kemudian kita lakukan pengurutan pada kolom ke-1 (rank) secara menaik, setelah itu diambil 5 data teratas dan tampilkan nama universitasnya saja  
- Untuk soal (b), pertama kita cari dahulu universitas yang berada di Jepang, kemudian lakukan pengurutan pada kolom ke-9 (fsr score) secara menaik, ambil 5 data teratas dan tampilkan nama universitasnya saja
- Untuk soal (c), pertama kita cari dahulu universitas yang berada di Jepang, kemudian kita lakukan pengurutan pada kolom ke-20 (ger rank) secara menaik, setelah itu diambil 10 data teratas dan tampilkan nama universitasnya saja  
- Untuk soal (d), pertama kita cari universitas yang namanya mengandung suatu variasi dari kata "keren" kemudian tampilkan nama universitasnya saja


Kendala : Dalam pengerjaan soal (c), rank GER sering tercampur aduk. Pada akhirnya kami menggunakan pendekatan lain untuk menyelesaikannya
# No 2
Soal ini memiliki 2 bagian 
1) Download gambar tentang Indonesia sebanyak X kali dimana X adalah jam pada saat script diexecute. Apabila script diexecute pada jam 00.00 maka cukup mendownload 1 gambar saja. Gambar didownload setiap 10 jam sekali. Semua gambar yang didownload pada 1 waktu dimasukkan kedalam 1 folder.
   - Format penamaan gambar adalah perjalanan_NOMOR.FILE
   - Format penamaan folder adalah kumpulan_NOMOR.FOLDER
2) Zip folder yang berisi gambar setiap 1 hari dengan format nama zip file devil_NOMOR.ZIP.

Untuk menyelesaikan persoalan tersebut dapat mengikuti langkah berikut:

1. Buat file `kobeni_liburan.sh` dengan menggunakan command `nano kobeni_liburan.sh`  
![buat file script](images/2_1.png)
2. Tambahkan teks berikut pada script tersebut
```shell
#!/bin/bash

#crontab used
#0 */10 * * * /bin/bash /home/vagrant/Documents/sisop/praktikum_1/soal2/kobeni_liburan.sh
#30 23 * * * /bin/bash /home/vagrant/Documents/sisop/praktikum_1/soal2/kobeni_liburan.sh

HOUR=$(TZ=Asia/Jakarta date +"%H")
MINUTE=$(TZ=Asia/Jakarta date +"%M") 

DIR="/home/vagrant/Documents/sisop/praktikum_1/soal2/"

if [ "$HOUR" -eq 23 ] && [ "$MINUTE" -eq 30 ]; then
    	last_zip=$(ls -1d "$DIR"devil_* | sort -Vr | head -n 1)
	if [ -z "$last_zip" ]
	then
    		num_zip=1
	else
    		num_zip=$(basename "$last_zip" | sed 's/devil_//' | sed 's/\.zip//')
    		num_zip=$((num_zip + 1))
	fi
	new_zip="${DIR}devil_${num_zip}"
	mkdir "$new_zip"
	find "$DIR" -maxdepth 1 -type d -name "kumpulan*" -exec mv {} "$new_zip" \;

	zip_file="${new_zip}.zip"
	zip -r "$zip_file" "devil_${num_zip}"
	rm -rf "$new_zip"
	exit 0
fi 

last_folder=$(ls -1d "$DIR"kumpulan_* | sort -Vr | head -n 1)
if [ -z "$last_folder" ]
then
    num_folder=1
else
    num_folder=$(basename "$last_folder" | sed 's/kumpulan_//')
    num_folder=$((num_folder + 1))
fi

new_folder="${DIR}kumpulan_${num_folder}"

mkdir "$new_folder"

if [ "$HOUR" -eq 0 ]; then
	num_images=1
else 
 	num_images=$HOUR
fi

for (( i=1; i<=$num_images; i++ )); do
  wget -O "$new_folder/perjalanan_$i.jpg" "https://source.unsplash.com/random/?indonesia"
done
```  
![isi file script](images/2_2.png)

3. Buka window cronjob dengan command `crontab -e`  
4. Tambahkan teks berikut ke dalam file cronjob
```shell
0 */10 * * * /bin/bash /home/vagrant/Documents/sisop/praktikum_1/soal2/kobeni_liburan.sh
30 23 * * * /bin/bash /home/vagrant/Documents/sisop/praktikum_1/soal2/kobeni_liburan.sh
```  
![isi file cronjob](images/2_4.png)

Penjelasan kode

- Inisialisasi Directory yang digunakan
`DIR="/home/vagrant/Documents/sisop/praktikum_1/soal2/"` 
- Simpan jam dan menit pada saat script dijalankan.  
Ket: Menggunakan format sebagai berikut  
`HOUR=$(TZ=Asia/Jakarta date +"%H")`  
`MINUTE=$(TZ=Asia/Jakarta date +"%M")`
- Cek apabila waktu script dijalankan adalah waktu untuk membuat zip file. Di file kami, waktu yang ditentukan adalah 23.30 untuk membuat zip file.
  - Waktu ini ditentukan di file cronjob juga sebagai berikut:  `30 23 * * *`
  - Menggunakan kondisi if sebagai berikut: `[ "$HOUR" -eq 23 ] && [ "$MINUTE" -eq 30 ]`  

- Ambil nama zip file sebelumnya jika ada. Apabila tidak ada, maka penamaan zip file dimulai dari 1. Apabila sudah ada, maka ambil angka dari zip file sebelumnya dan tambahkan.  
Ket: Menggunakan `ls -1d "$DIR"devil_*` untuk mengambil setiap file yang memiliki substring devil_, lalu menggunakan `sort -Vr` untuk mengambil angka terbesar pada file file tersebut, dan diakhiri dengan `head -n 1` untuk mendapatkan 1 file saja dengan angka terbesar.
- Untuk mengambil angka terakhir dari kumpulan `Devil_` yang sudah ada, dapat menggunakan perrintah `num_zip=$(basename "$last_zip" | sed 's/devil_//' | sed 's/\.zip//')` dimana `basename` digunakan untuk mengambil alamat file terakhir, jika di dalam kasus ini adalah `devil_*.zip`. setelah itu menggunakan perintah `sed 's/devil_//' | sed 's/\.zip//')` untuk menghapus `devil_` di awal dan `.zip` di akhir, sehingga menyisakan angka saja, dari angka yang didapatkan, diincrement untuk menjadi parameter angka berikutnya.
- Untuk menambahkan semua folder `kumpulan_` ke dalam zip `devil_`, mengunakan perintah `find "$DIR" -maxdepth 1 -type d -name "kumpulan*" -exec mv {} "$new_zip"`. Perintah ini mencari di lokasi directory `soal2`, yang memiliki penamaan file `kumpulan` dan dipindahkan ke folder `$new_zip`
- Setelah semua folder `kumpulan_` dimasukkan kedalam folder `$new_zip`, folder kemudian di zip dengan perintah `zip -r "$zip_file" "devil_${num_zip}"` dan folder sebelumnya dihapus dengan perintah `rm -rf "$new_zip"`

- Apabila jam pada saat script dijalankan bukan 23.30, maka akan berlanjut pada bagian pertama soal, yaitu mendownload gambar.
- Dimulai dengan mendapatkan nama folder sebelumnya jika ada dan menambahkan angka pada nama folder tersebut, mirip dengan proses penamaan zip file.
Ket: Menggunakan `ls -1d "$DIR"kumpulan_* ` untuk mendapatkan nama folder dengan prefix kumpulan. Command selanjutnya sama persis dengan proses pengambilan index zip file.
- Setelah folder dibuat menggunakan `"${DIR}kumpulan_${num_folder}"`, variabel penyimpan jam digunakan untuk menentukan berapa banyak gambar yang akan didownload. Apabila jam 00.xx, maka gambar yang didownload hanya 1.
- Command yang berfungsi untuk mendownload file adalah sebagai berikut:  
`wget -O "$new_folder/perjalanan_$i.jpg"`  
dimana argumen -O digunakan untuk menempatkan dan menamai file sesuai dengan format penamaan.
- URL yang digunakan adalah sebagai berikut:
`"https://source.unsplash.com/random/?indonesia"`
untuk mendownload gambar yang berbeda pada setiap iterasi loop.
- Penamaan setiap gambar menggunakan variabel increment loop.
 

Contoh Hasil:

- Output saat script sudah mendownload gambar  
![output download gambar](images/2_5.png)
- Output saat script membuat zip file  
![output zip file](images/2_6.png)  

- Hasil folder kumpulan dan zip

![output download gambar](images/2_7.png)
- Hasil gambar yang terdownload

![output hasil](images/2_8.png)
- Hasil saat jam 23.30 (zip setiap kumpulan yang ada di hari tersebut) 

![output zip file](images/2_9.png)  
- Isi dari zip_file devil_4.zip

![isi devil_4.zip file](images/2_10.png)  

Kendala :  sempat terkendala pada link download gambar yang hanya mendownload gambar yang sama berulang-ulang, setelah itu mengganti link source sebagai random gambar, sehingga kendala dapat terselesaikan.

# No 3  
Soal ini memiliki 2 bagian  
1) Membuat sistem registrasi di dalam file `louis.sh` dan  sistem login di dalam file `retep.sh`. Setiap registrasi sukses disimpan dalam `users/users.txt`. Password memiliki ketentuan sebagai berikut:
   - Minimal 8 karakter
   - Memiliki minimal 1 huruf kapital dan 1 huruf 1 kecil
   - Alphanumeric/terdiri dari angka dan abjad
   - Tidak boleh sama dengan username
   - Tidak boleh menggunakan `chicken` atau `ernie`   
3) Setiap percobaan registrasi dan login dicatat kedalam `log.txt` dengan format `YY/MM/DD hh:mm:ss MESSAGE`. MESSAGE akan menyesuaikan aksi user.
   - Registrasi dengan username yang sudah terdaftar = `REGISTER: ERROR User already exists`  
   - Registrasi berhasil = `REGISTER: INFO User USERNAME registered successfully`  
   - Login dengan password salah = `LOGIN: ERROR Failed login attempt on user USERNAME`  
   - Login berhasil = `LOGIN: INFO User USERNAME logged in`  

Menggunakan langkah berikut:  
1. Buat file `louis.sh` dan `retep.sh` menggunakan `nano` 
![buat file script](images/3_1.png) 
2. Tambahkan teks berikut pada `louis.sh`  
```shell
#!/bin/bash

DIR="/home/vagrant/Documents/sisop/praktikum_1/soal3/"

echo "Masukkan username:"
read username

if grep -q "^$username:" "$DIR"users/users.txt; then
    echo "User sudah terdaftar."
    echo "$(date +'%Y/%m/%d %H:%M:%S') REGISTER: ERROR User $username already exists" >> log.txt
    exit 1
fi

read -s -p "Masukkan password: " password
echo 

flag=1
# Validasi password
if [[ ${#password} -lt 8 ]]; then
  echo "Password harus memiliki minimal 8 karakter."
  flag=0
fi

if ! [[ $password =~ [A-Z] ]]; then
  echo "Password harus memiliki minimal 1 huruf kapital."
  flag=0
fi

if ! [[ $password =~ [a-z] ]]; then
  echo "Password harus memiliki minimal 1 huruf kecil."
  flag=0
fi

if ! [[ $password =~ ^[a-zA-Z0-9]+$ ]]; then
  echo "Password hanya boleh mengandung karakter alphanumeric."
  flag=0
fi

if [[ $password == *$username* ]]; then
  echo "Password tidak boleh sama dengan username."
  flag=0
fi

if [[ $password == *"chicken"* || $password == *"ernie"* ]]; then
  echo "Password tidak boleh mengandung kata 'chicken' atau 'ernie'."
  flag=0
fi

if [[ $flag == 0 ]]; then
  exit 1
fi

echo "Password memenuhi persyaratan."
echo
echo "$username:$password" >>  "$DIR"users/users.txt
echo "User berhasil didaftarkan."
echo "$(date +'%Y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> log.txt
exit 0

```
![isi file louis.sh](images/3_2.png)
![isi file louis.sh](images/3_3.png)  
  
dan teks berikut pada `retep.sh` 
```shell
#!/bin/bash

DIR="/home/vagrant/Documents/sisop/praktikum_1/soal3/"

echo "Masukkan username:"
read username

if ! grep -q "^$username:" "$DIR"users/users.txt; then
    echo "Username tidak ditemukan."
    echo "$(date +'%Y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    exit 1
fi

echo "Masukkan password:"
read -s password

if ! grep -q "^$username:$password$" "$DIR"users/users.txt; then
    echo "Password salah."
    echo "$(date +'%Y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    exit 1
fi

echo "Selamat datang, $username."
echo "$(date +'%Y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt
exit 0

```  
![isi file retep.sh](images/3_4.png)  
  
3. Jalankan menggunakan `bash louis.sh` dan `bash retep.sh`  

Penjelasan kode `louis.sh` :  
- Langkah pertama Inisialisasi Directory yang digunakan
`DIR="/home/vagrant/Documents/sisop/praktikum_1/soal3/"` 
- Langkah selanjutnya adalah mengecek apabila username yang dimasukkan sudah ada di `users/users.txt` atau tidak menggunakan grep sebagai berikut:  `grep -q "^$username:" "$DIR"users/users.txt;`  
- Apabila sudah ada, maka script memberi tahu user dengan echo bahwa username yang diinputkan sudah terdaftar/registrasi.    
Format log yang dipakai adalah sebagai berikut :
`echo "$(date +'%Y/%m/%d %H:%M:%S') REGISTER: ERROR User $username already exists" >> log.txt`
- Sistem pengecekan validasi password menggunakan flag dan banyak if-statement. Apabila salah satu saja dari if-statement tidak terpenuhi, flag yang awalnya diisi 1 akan diset menjadi 0, menggagalkan proses registrasi.
   - Kondisi minimal 8 karakter = `${#password} -lt 8`
   - Kondisi minimal 1 huruf kapital = `$password =~ [A-Z]`
   - Kondisi minimal 1 huruf kecil = `$password =~ [a-z]`
   - Kondisi hanya alphanumeric = `$password =~ ^[a-zA-Z0-9]+$ `
   - Kondisi tidak boleh sama dengan username = `$password == *$username*`
   - Kondisi tidak boleh mengandung kata 'ernie' dan 'chicken' = `$password == *"chicken"* || $password == *"ernie"*`
   - Text yang dioutputkan ke log file pada saat user berhasil registrasi = `echo "$(date +'%Y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> log.txt`
- Penggunaan -s pada pengambilan password pada `read -s -p "Masukkan password: " password` digunakan untuk menyembunyikan input user.

Penjelasan kode `retep.sh`:
- Langkah pertama adalah memastikan username yang diinputkan sudah ada di `users/users.txt` menggunakan grep sebagai berikut:  
`grep -q "^$username:" "$DIR"users/users.txt` 
- Apabila tidak ditemukan, maka script mengoutputkan ke log file sebagai berikut:  
`echo "$(date +'%Y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt`
- Apabila username sudah benar, password akan dicek dengan grep sebagai berikut:  
`grep -q "^$username:$password$" "$DIR"users/users.txt` 
- Password yang salah akan mengoutputkan text berikut pada log file:  
`echo "$(date +'%Y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt` 
- Apabila username dan password sudah benar, maka user bisa masuk dan disambut oleh script. Text yang dimasukkan ke dalam log file adalah sebagai berikut:  
`echo "$(date +'%Y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt`

Contoh hasil `louis.sh` :  
  
![gagal regis](images/3_5.png)  
![sukses regis](images/3_6.png)
  
Contoh hasil `retep.sh` :   
  
![login](images/3_7.png)

Isi dari `log.txt` dan `users.txt` :
  
![logfile dan users](images/3_8.png)
  
Kendala : sempat terkendala pada bagian akses directory ke `users/users.txt`, problem terselesaikan ketika menginisialisasi directory di awal, yang kemudian ditetapkan sebagai variable `DIR`


# No 4  
Soal ini menginginkan 3 hal  
1) Backup file syslog menjadi extensi .txt dengan format penamaan  
`jam:menit tanggal:bulan:tahun`
2) Enkripsikan file tersebut dengan menggunakan Caesar Cipher (shift) sebanyak X tempat ke kanan apabila sekarang merupakan jam X (format 24 jam). Gunakan script bernama `log_encrypt.sh`
3) Dekripsikan file tersebut dengan menggunakan Caesar Cipher (shift) sebanyak X tempat ke kiri apabila sekarang merupakan jam X (format 24 jam). Gunakan script bernama `log_decrypt.sh`
4) Backup file syslog setiap 2 jam  

Perlu diperhatikan bahwa akan perlu digunakan absolute path untuk semua input/output ke suatu file sebab syslog terletak di folder /var/... sedangkan script berada di /home/...

Untuk menyelesaikan persoalan ini, dapat diikuti langkah-langkah berikut:  
1) Buat file `log_encrypt.sh` dengan menggunakan command  
`nano log_encrypt.sh`  
![buat file script enkripsi](images/4_1.jpeg)  
2) Tambahkan text berikut ke script enkripsi tersebut  
```shell
#!/bin/bash

cp /var/log/syslog /home/richard160421084/Documents/Sisop/Prak1/Tugas/4/"$(date +\%H:\%M) $(date +\%d:\%m:\%Y)".txt

alphabet="abcdefghijklmnopqrstuvwxyz"
caps="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

hour=$(date +%H)

encoded_alphabet="${alphabet:$hour}${alphabet:0:$hour}"
encoded_caps="${caps:$hour}${caps:0:$hour}"

input_file=/home/richard160421084/Documents/Sisop/Prak1/Tugas/4/"$(date +\%H:\%M) $(date +\%d:\%m:\%Y)".txt

encoded_message=$(cat "$input_file" | while read line; do echo "$line" | tr "$alphabet" "$encoded_alphabet" | tr "$caps" "$encoded_caps" ; done);

echo "$encoded_message" >> /home/richard160421084/Documents/Sisop/Prak1/Tugas/4/"$(date +\%H:\%M) $(date +\%d:\%m:\%Y)"_encrypted.txt

bash log_decrypt.sh
```
![isi script enkripsi](images/4_2.jpeg)  
3) Buat file `log_decrypt.sh` dengan menggunakan command  
`nano log_decrypt.sh`  
![buat file script dekripsi](images/4_3.jpeg)  
4) Tambahkan text berikut ke script dekripsi tersebut  
```shell
#!/bin/bash

alphabet="abcdefghijklmnopqrstuvwxyz"
caps="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

hour=$(date +%H)

#Shift 5 ke kiri = shift 21 ke kanan
hour=26-$hour

encoded_alphabet="${alphabet:$hour}${alphabet:0:$hour}"
encoded_caps="${caps:$hour}${caps:0:$hour}"

input_file=/home/richard160421084/Documents/Sisop/Prak1/Tugas/4/"$(date +\%H:\%M) $(date +\%d:\%m:\%Y)"_encrypted.txt

decoded_message=$(cat "$input_file" | while read line; do echo "$line" | tr "$alphabet" "$encoded_alphabet" | tr "$caps" "$encoded_caps" ; done);

echo "$decoded_message" >> /home/richard160421084/Documents/Sisop/Prak1/Tugas/4/"$(date +\%H:\%M) $(date +\%d:\%m:\%Y)"_decrypted.txt
```
![isi script dekripsi](images/4_4.jpeg)  
5) Secara default, file tidak memiliki permission untuk dijalankan maka kita harus menambahkan permission untuk execute script tersebut dengan menggunakan command  
`chmod +x log_encrypt.sh` dan `chmod +x log_decrypt.sh`  
![tambah permission](images/4_5.jpeg)  
6) Buka window cronjob dengan menggunakan command `crontab -e`  
![buka cronjob](images/4_6.jpeg)  
7) Tambahkan text berikut ke window cronjob tadi  
```shell
0 */2 * * * /bin/bash /home/richard160421084/Documents/Sisop/Prak1/Tugas/4/log_encrypt.sh
```
![isi cronjob](images/4_7.jpeg)  
Keterangan : gunakan absolute path untuk file enkripsi  

Penjelasan code
- Pertama tama, kita backup terlebih dahulu file syslog yang terdapat pada folder /var/log/syslog ke folder dimana kita akan bekerja dengan menggunakan format penamaan yang telah ditentukan  
Perlu digunakan absolute path karena folder awal syslog berasal dari root
- Deklarasikan key awal yang kemudian akan digunakan untuk pemetaan pergeseran  
- Dapatkan jumlah pergeseran yang perlu dilakukan  
Ket: Apabila saat ini sedang proses decode, maka gunakan pergeseran sebesar `26-hour` karena pergeseran ke kiri sebanyak `hour` = pergeseran ke kanan sebanyak `26-hour`
- Dapatkan hasil pergeseran dari key awal tadi  
`${alphabet:$hour}` akan menghasilkan substring dari variabel `alphabet` mulai dari index `hour`  
`${alphabet:0:$hour}` akan menghasilkan substring dari variabel `alphabet` mulai dari index 0 hingga index `hour-1`  
Kemudian, kedua substring ini kita gabungkan (pastikan tidak terdapat whitespace diantara kedua string ini)  
Hal yang sama kita lakukan untuk bagian huruf kapital  
- Langkah ini merupakan langkah yang opsional. Deklarasikan path ke file backup tadi menjadi variabel untuk kemudahan code selanjutnya  
- Enkripsi / Dekripsikan file backup tadi  
`cat "$input_file"` akan memberikan isi dari file yang ditunjuk oleh variabel `input_file` dalam plaintext  
`while read line; do echo "$line"` akan memberikan isi dari plaintext sebelumnya dalam baris per baris dalam bentuk plaintext
`tr "$alphabet" "$encoded_alphabet"` akan melakukan mapping dari string hasil `echo $line` dengan menggunakan key awal yaitu string `alphabet` dan string akhir `encoded_alphabet`
- Print file hasil enkripsi / dekripsi ke suatu file lain untuk kemudahan pembacaan dan penyimpanan dengan menggunakan command  
`echo "$encoded_message" >> file.txt`  
Keterangan: Pisahkan file hasil enkripsi dan file  

Contoh hasil :  
![daftar file](images/4_8.jpeg)  
Awal  
![syslog awal](images/4_9.jpeg)  
Encrypted  
![encrypted syslog](images/4_10.jpeg)  
Decrypted  
![decrypted syslog](images/4_11.jpeg)  


Kendala : 
1) Setelah dilakukan enkripsi / deksripsi, file menjadi hanya 1 baris saja. Permasalahan ditemukan pada baris terakhir yaitu `echo $encoded_message >> file.txt`, akhirnya kami ubah baris tersebut menjadi `echo "$encoded_message" >> file.txt`  
2) File bisa dijalankan secara manual akan tetapi gagal berjalan ketika dijalankan oleh cronjob. Setelah dilakukan debugging ditemukan bahwa file input untuk enkripsi maupun dekripsi tidak ditemukan. Akhirnya untuk kedua file tersebut kami gunakan absolute path untuk seluruh input/output ke file 

#!/bin/bash

#crontab
#0 */10 * * * /bin/bash /home/vagrant/Documents/sisop/praktikum_1/soal2/kobeni_liburan.sh
#30 23 * * * /bin/bash /home/vagrant/Documents/sisop/praktikum_1/soal2/kobeni_liburan.sh


HOUR=$(TZ=Asia/Jakarta date +"%H")
MINUTE=$(TZ=Asia/Jakarta date +"%M") 

DIR="/home/vagrant/Documents/sisop/praktikum_1/soal2/"

if [ "$HOUR" -eq 23 ] && [ "$MINUTE" -eq 30 ]; then
    	last_zip=$(ls -1d "$DIR"devil_* | sort -Vr | head -n 1)
	if [ -z "$last_zip" ]
	then
    		num_zip=1
	else
    		num_zip=$(basename "$last_zip" | sed 's/devil_//' | sed 's/\.zip//')
    		num_zip=$((num_zip + 1))
	fi
	new_zip="${DIR}devil_${num_zip}"
	mkdir "$new_zip"
	find "$DIR" -maxdepth 1 -type d -name "kumpulan*" -exec mv {} "$new_zip" \;

	zip_file="${new_zip}.zip"
	zip -r "$zip_file" "devil_${num_zip}"
	rm -rf "$new_zip"
	exit 0
fi 

last_folder=$(ls -1d "$DIR"kumpulan_* | sort -Vr | head -n 1)
if [ -z "$last_folder" ]
then
    num_folder=1
else
    num_folder=$(basename "$last_folder" | sed 's/kumpulan_//')
    num_folder=$((num_folder + 1))
fi

new_folder="${DIR}kumpulan_${num_folder}"

mkdir "$new_folder"

if [ "$HOUR" -eq 0 ]; then
	num_images=1
else 
 	num_images=$HOUR
fi

for (( i=1; i<=$num_images; i++ )); do
  wget -O "$new_folder/perjalanan_$i.jpg" "https://source.unsplash.com/random/?indonesia"
done

#!/bin/bash

DIR="/home/vagrant/Documents/sisop/praktikum_1/soal3/"

echo "Masukkan username:"
read username

if ! grep -q "^$username:" "$DIR"users/users.txt; then
    echo "Username tidak ditemukan."
    echo "$(date +'%Y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    exit 1
fi

echo "Masukkan password:"
read -s password

if ! grep -q "^$username:$password$" "$DIR"users/users.txt; then
    echo "Password salah."
    echo "$(date +'%Y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    exit 1
fi

echo "Selamat datang, $username."
echo "$(date +'%Y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt
exit 0

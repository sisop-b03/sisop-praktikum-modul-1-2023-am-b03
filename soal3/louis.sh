#!/bin/bash

DIR="/home/vagrant/Documents/sisop/praktikum_1/soal3/"

echo "Masukkan username:"
read username

if grep -q "^$username:" "$DIR"users/users.txt; then
    echo "User sudah terdaftar."
    echo "$(date +'%Y/%m/%d %H:%M:%S') REGISTER: ERROR User $username already exists" >> log.txt
    exit 1
fi

read -s -p "Masukkan password: " password
echo 

flag=1
# Validasi password
if [[ ${#password} -lt 8 ]]; then
  echo "Password harus memiliki minimal 8 karakter."
  flag=0
fi

if ! [[ $password =~ [A-Z] ]]; then
  echo "Password harus memiliki minimal 1 huruf kapital."
  flag=0
fi

if ! [[ $password =~ [a-z] ]]; then
  echo "Password harus memiliki minimal 1 huruf kecil."
  flag=0
fi

if ! [[ $password =~ ^[a-zA-Z0-9]+$ ]]; then
  echo "Password hanya boleh mengandung karakter alphanumeric."
  flag=0
fi

if [[ $password == *$username* ]]; then
  echo "Password tidak boleh sama dengan username."
  flag=0
fi

if [[ $password == *"chicken"* || $password == *"ernie"* ]]; then
  echo "Password tidak boleh mengandung kata 'chicken' atau 'ernie'."
  flag=0
fi

if [[ $flag == 0 ]]; then
  exit 1
fi

echo "Password memenuhi persyaratan."
echo
echo "$username:$password" >>  "$DIR"users/users.txt
echo "User berhasil didaftarkan."
echo "$(date +'%Y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> log.txt
exit 0
